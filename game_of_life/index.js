
const unitLength = 40;
const boxColor = 150;
const strokeColor = 50;
let columns; /* To be determined by window width */
let rows;    /* To be determined by window height */
let currentBoard;
let nextBoard;
let bgColor = 255;
let cellColor = "rgb(0, 191, 255)";
let changeOfSurvival = 3;
let changeOfReproduction = 3;
let frame = 20;
let cellTransparency = 0.25;
let cellRatio = 1.03;
let allColor;




function setup() {
    /* Set the canvas to be under the element #canvas*/
    const canvas = createCanvas(windowWidth, windowHeight - 100);
    canvas.parent(document.querySelector('#canvas'));




    scroll = 0;

    frameRate(frame);
    /*Calculate the number of columns and rows */
    columns = floor(windowWidth / unitLength);
    rows = floor(windowHeight - 100 / unitLength);

    /*Making both currentBoard and nextBoard 2-dimensional matrix that has (columns * rows) boxes. */
    currentBoard = [];
    nextBoard = [];
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = []
    }
    // Now both currentBoard and nextBoard are array of array of undefined values.
    init();  // Set the initial values of the currentBoard and nextBoard
    noLoop();
}


function init() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = 0;
            nextBoard[i][j] = 0;
        }
    }
}

function draw() {
    background(255);
    generate();
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {



            if (currentBoard[i][j] == 0) {
                fill(255);
            } else if (currentBoard[i][j] == 1) {
                fill('black');
            } else {
                fill(multipleColor(currentBoard[i][j]));
            }
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
}

function generate() {
    //Loop over every single box on the board
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            // Count all living members in the Moore neighborhood(8 boxes surrounding)
            let neighbors = 0;
            for (let i of [-1, 0, 1]) {
                for (let j of [-1, 0, 1]) {
                    if (i == 0 && j == 0) {
                        // the cell itself is not its own neighbor
                        continue;
                    }
                    // The modulo operator is crucial for wrapping on the edge

                    let cellValue = Math.ceil(currentBoard[(x + i + columns) % columns][(y + j + rows) % rows]);
                    neighbors += cellValue

                }
            }

            // Rules of Life
            if (currentBoard[x][y] != 0 && neighbors < 2) {
                // Die of Loneliness
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] != 0 && neighbors > changeOfSurvival) {
                // Die of Overpopulation
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 0 && neighbors == changeOfReproduction) {
                // New life due to Reproduction
                nextBoard[x][y] = cellTransparency;
            } else {
                // Stasis
                nextBoard[x][y] = Math.min(1, currentBoard[x][y] * cellRatio);
            }
        }
    }

    // Swap the nextBoard to be the current Board
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}

function mouseDragged() {
    /**
     * If the mouse coordinate is outside the board
     */
    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    currentBoard[x][y] = cellTransparency;
    fill("green");
    stroke("green");
    rect(x * unitLength, y * unitLength, unitLength, unitLength);
}

/**
 * When mouse is pressed
 */
function mousePressed() {
    //noLoop();
    mouseDragged();





}

/**
 * When mouse is released
 */
function mouseReleased() {
    //loop();
}

const pattern3 = [


    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 0, 0, 0],


    [0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],

    [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0],

    [0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1],
    [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1],
    [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0],
    [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],





];

const pattern2 = [

    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 1, 1, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 1, 0, 0, 0, 0],
    [0, 1, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0, 0],



];
let patterString = `
-XXX-XXX--XXX
XX-XXXX-XXXXX
-XXX-XXXXX-XX
`



function generatePatternArray(patterString) {
    let patternArray = patterString.split('\n')
    patternArray.pop()
    patternArray.shift()
    console.log('`````````````````````');
    let row = []
    let col = []
    for (let line = 0; line < patternArray.length; line++) {
        for (let cell = 0; cell < patternArray[line].length; cell++) {
            let cellVal = patternArray[line][cell]
            if (cellVal === '-') {
                row.push(0)
            } else {
                row.push(1)

            }
        }
        col.push(row)
    }

    return col
}



const pattern = [
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],

    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 0, 1, 0, 0, 0,],
    [0, 0, 0, 0, 0, 0, 0, 0],


    [0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 0, 1, 0, 1, 0, 1],
    [0, 0, 0, 0, 0, 1, 0, 1, 0, 1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],

    [0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],

    [0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 0, 0, 1, 0, 1],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 1],



    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
    [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0, 0],


];







document.querySelector('#pattern').addEventListener('click', function () {
    for (let i = 0; i < pattern.length; i++) {
        for (let j = 0; j < pattern[i].length; j++) {
            if (pattern[i][j] == 1) {
                currentBoard[i][j] = 1
            } else {
                currentBoard[i][j] = 0
            }

        }

    };

    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1) {


                fill("black");


            } else {

                fill(multipleColor(currentBoard[i][j]));
                //fill(`rgba(256,0,0, ${currentBoard[i][j]})`);

            }
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
});

document.querySelector('#pattern2').addEventListener('click', function () {
    for (let i = 0; i < pattern2.length; i++) {
        for (let j = 0; j < pattern2[i].length; j++) {
            if (pattern2[i][j] == 1) {
                currentBoard[i][j] = 1
            } else {
                currentBoard[i][j] = 0
            }

        }

    };

    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1) {


                fill("black");


            } else {

                fill(multipleColor(currentBoard[i][j]));
                //fill(`rgba(256,0,0, ${currentBoard[i][j]})`);

            }
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
});

document.querySelector('#pattern3').addEventListener('click', function () {
    for (let i = 0; i < pattern3.length; i++) {
        for (let j = 0; j < pattern3[i].length; j++) {
            if (pattern3[i][j] == 1) {
                currentBoard[i][j] = 1
            } else {
                currentBoard[i][j] = 0
            }

        }

    };

    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1) {


                fill("black");


            } else {

                fill(multipleColor(currentBoard[i][j]));
                //fill(`rgba(256,0,0, ${currentBoard[i][j]})`);

            }
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
});


let survival = document.querySelector('#survival');

survival.addEventListener('input', function () {


    changeOfSurvival = survival.value

});


let reproduction = document.querySelector('#reproduction');

reproduction.addEventListener('input', function () {


    changeOfReproduction = reproduction.value

});

let frameRateRange = document.querySelector('#slider');

frameRateRange.addEventListener('input', function () {


    frameRate(parseInt(frameRateRange.value));
});






function multipleColor(alpha) {

    let randomcolor1 = Math.floor(Math.random() * 255)
    let randomcolor2 = Math.floor(Math.random() * 255)
    let randomcolor3 = Math.floor(Math.random() * 255)


    let allColor = "rgba(" + randomcolor1 + "," + randomcolor2 + "," + randomcolor3 + "," + alpha + ")";

    return allColor;

};


document.querySelector('#pattern').addEventListener('click', function () {
    for (let i = 0; i < pattern.length; i++) {
        for (let j = 0; j < pattern[i].length; j++) {
            if (pattern[i][j] == 1) {
                currentBoard[i][j] = 1
            } else {
                currentBoard[i][j] = 0
            }

        }

    };

    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1) {


                fill("black");


            } else {

                fill(multipleColor(currentBoard[i][j]));
                //fill(`rgba(256,0,0, ${currentBoard[i][j]})`);

            }
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
});


document.querySelector('#pattern2').addEventListener('click', function () {
    for (let i = 0; i < pattern2.length; i++) {
        for (let j = 0; j < pattern2[i].length; j++) {
            if (pattern2[i][j] == 1) {
                currentBoard[i][j] = 1
            } else {
                currentBoard[i][j] = 0
            }

        }

    };

    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1) {


                fill("black");


            } else {

                fill(multipleColor(currentBoard[i][j]));
                //fill(`rgba(256,0,0, ${currentBoard[i][j]})`);

            }
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
});

document.querySelector('#pattern3').addEventListener('click', function () {
    for (let i = 0; i < pattern3.length; i++) {
        for (let j = 0; j < pattern3[i].length; j++) {
            if (pattern3[i][j] == 1) {
                currentBoard[i][j] = 1
            } else {
                currentBoard[i][j] = 0
            }

        }

    };

    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1) {


                fill("black");


            } else {

                fill(multipleColor(currentBoard[i][j]));
                //fill(`rgba(256,0,0, ${currentBoard[i][j]})`);

            }
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
});
