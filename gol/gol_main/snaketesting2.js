function Snake() {

    this.x = 0;
    this.y = 0;
    this.xspeed = 1;
    this.yspeed = 0;


    this.update = function () {


        this.x = this.x + this.xspeed * 40;
        this.y = this.y + this.yspeed * 40;

        this.x = constrain(this.x, 0, width - scl);
        this.y = constrain(this.y, 0, height - scl);




    }


    this.dir = function (x, y) {

        this.xspeed = x;
        this.yspeed = y;
    }




    this.show = function () {


        fill(255);



        rect(this.x, this.y, 40, 40);
    }

}