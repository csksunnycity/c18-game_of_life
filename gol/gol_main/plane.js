let clouds;
let stars;
let scroll;

let planeX = 200;
let planeY = 200;

let isLeft;
let isRight;
let isUp;
let isDown;



function setup() {
    const canvas = createCanvas(document.querySelector('main').offsetWidth, document.querySelector('main').offsetHeight);
    canvas.parent(document.querySelector('#canvas'));

    createCanvas(2338, 500);
    scroll = 0;

    isLeft = false;
    isRight = false;
    isUp = false;
    isDown = false;

    clouds = [
        { x: -1000, y: 200, w: 100, h: 50 },
        { x: -300, y: 500, w: 150, h: 60 },
        { x: -500, y: 200, w: 100, h: 80 },
        { x: 100, y: 600, w: 150, h: 40 },
        { x: 300, y: 200, w: 100, h: 50 },
        { x: 700, y: 400, w: 140, h: 50 },
        { x: 1000, y: 100, w: 100, h: 30 },
        { x: 1600, y: 300, w: 300, h: 80 },
        { x: 2000, y: 500, w: 140, h: 60 }

    ]

    stars = [
        { a: 100, b: 100, c: 5 },
        { a: 300, b: 300, c: 8 },
        { a: 130, b: 320, c: 5 },
        { a: 300, b: 290, c: 8 },
        { a: 600, b: 200, c: 5 },
        { a: 380, b: 230, c: 5 },
        { a: 830, b: 130, c: 8 },
        { a: 830, b: 330, c: 5 },
        { a: 380, b: 307, c: 8 },
        { a: 309, b: 130, c: 5 },
        { a: 530, b: 160, c: 5 },
        { a: 1030, b: 250, c: 8 },
        { a: 930, b: 390, c: 8 },
        { a: 1300, b: 300, c: 8 },
        { a: 1030, b: 320, c: 5 },
        { a: 1500, b: 251, c: 5 },
        { a: 1600, b: 335, c: 8 },
        { a: 800, b: 368, c: 8 },
        { a: 830, b: 191, c: 5 },
        { a: 830, b: 330, c: 8 },
        { a: 980, b: 157, c: 5 },
        { a: 1309, b: 180, c: 5 },
        { a: 530, b: 161, c: 8 },



    ]







}

function draw() {
    background(135, 206, 205);
    push();
    translate(scroll, 0)

    for (let i = 0; i < clouds.length; i++) {
        fill(255);
        noStroke();
        ellipse(clouds[i].x, clouds[i].y - 20, clouds[i].w - 10, clouds[i].h)

        ellipse(clouds[i].x + 20, clouds[i].y, clouds[i].w, clouds[i].h)

        ellipse(clouds[i].x - 20, clouds[i].y, clouds[i].w, clouds[i].h)
    }

    pop();

    if (isLeft == true) {

        fill(175, 0, 0)
        ellipse(planeX, planeY, 150, 40)
        arc(planeX + 55, planeY - 8, 25, 65, PI, 0)

        fill(175)
        arc(planeX - 10, planeY - 20, 30, 70, PI, 0)
        arc(planeX - 9, planeY, 30, 100, 0, PI)

    } else {

        fill(175, 0, 0)
        ellipse(planeX, planeY, 150, 40)
        arc(planeX - 55, planeY - 8, 25, 65, PI, 0)

        fill(175)
        arc(planeX - 10, planeY - 20, 30, 70, PI, 0)
        arc(planeX - 9, planeY, 30, 100, 0, PI)
    }

    if (isUp == true) {

        background(29, 75, 102);

        translate(scroll, 0)

        for (let i = 0; i < stars.length; i++) {
            fill(255);
            noStroke();
            circle(stars[i].x, clouds[i].y - 20, clouds[i].w - 10, clouds[i].h)

            circle(clouds[i].x + 20, clouds[i].y, clouds[i].w, clouds[i].h)

            circle(clouds[i].x - 20, clouds[i].y, clouds[i].w, clouds[i].h)
        }








    } else {
        fill(175, 0, 0)
        ellipse(planeX, planeY, 150, 40)
        arc(planeX - 55, planeY - 8, 25, 65, PI, 0)

        fill(175)
        arc(planeX - 10, planeY - 20, 30, 70, PI, 0)
        arc(planeX - 9, planeY, 30, 100, 0, PI)
    }


    if (isUp) {
        if (plane > width * 0.2) {
            planeY -= 5

        } else {

            scroll += 5
        }

    }



    if (isLeft) {
        if (plane > width * 0.2) {
            planeX -= 5

        } else {

            scroll += 5
        }

    }

    if (isRight) {
        if (plane < width * 0.8) {
            planeX += 5


        } else {

            scroll -= 5
        }

    }

}

function keyPressed() {
    if (keyCode == LEFT_ARROW) {

        isLeft = true;
    }
    if (keyCode == RIGHT_ARROW) {

        isRight = true;
    }
    if (keyCode == UP_ARROW) {

        isUp = true;
    }
    if (keyCode == DOWN_ARROW) {

        isDown = true;
    }

}

function keyReleased() {

    if (keyCode == LEFT_ARROW) {

        isLeft = false;
    }
    if (keyCode == RIGHT_ARROW) {

        isRight = false;
    }
    if (keyCode == UP_ARROW) {

        isUp = false;
    }
    if (keyCode == DOWN_ARROW) {

        isDown = false;
    }


}
